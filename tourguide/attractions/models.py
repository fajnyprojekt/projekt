from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from geoposition.fields import GeopositionField
from djmoney.models.fields import MoneyField


# Create your models here.


class Category(models.Model):
    c_name = models.CharField(max_length=40)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.c_name


class Attraction(models.Model):
    a_name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, related_name="attractions")
    address = models.CharField(max_length=200)
    open_time = models.TimeField(max_length=4)
    close_time = models.TimeField(max_length=4)
    visit_time = models.DurationField()
    price = MoneyField(max_digits=5, decimal_places=2, default_currency='PLN')
    description = models.TextField()
    position = GeopositionField(default="52, 21")

    def cannot_set_negative_price(self):
        if int(self.price) < 0:
            self.price = '0'

    def __str__(self):
        return self.a_name


class Event(models.Model):
    attraction = models.ForeignKey(Attraction)
    e_name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    date = models.DateField(default=datetime.now, blank=True)
    open_time = models.TimeField(default=datetime.now, max_length=4)
    close_time = models.TimeField(default=datetime.now, max_length=4)

    def __str__(self):
        return self.e_name


class Route(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    route_name = models.CharField(max_length=50)
    attractions = models.ManyToManyField(Attraction, related_name='attractions')
    start = models.ForeignKey(Attraction, related_name='start', default=None, blank=True, null=True)
    end = models.ForeignKey(Attraction, related_name='end', default=None, blank=True, null=True)

    def __str__(self):
        return self.route_name


class Tourist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    routes = models.ManyToManyField(Route, related_name='routes')
