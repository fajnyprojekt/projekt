from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
# Register your models here.
from .models import *


class TouristInline(admin.StackedInline):
    model = Tourist
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (TouristInline, )


admin.site.register(Attraction)
admin.site.register(Event)
admin.site.register(Category)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Route)


