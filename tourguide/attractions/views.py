from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .forms import UserForm
from .models import Attraction
from .models import Event
from datetime import datetime
import calendar
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from .models import Attraction, Event, Route
import pdfkit
from django.core.urlresolvers import reverse


# Create your views here.

def stworzPDF(request, droga_id):
    url_moj = request.build_absolute_uri(reverse('drogi'))
    url_moj = url_moj+droga_id+'/'
    config = pdfkit.configuration(wkhtmltopdf='C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe')
    pdfkit.from_url("'"+url_moj+"'", 'out2.pdf', configuration=config)
    return render(request, 'attractions/index.html', {'url_moj' : url_moj})

def index(request):
    return render(request, 'attractions/index.html', {})


def atrakcje(request):
    lista_atrakcji = Attraction.objects.order_by('category')
    muzea = Attraction.objects.filter(category=1)
    sakralne = Attraction.objects.filter(category=2)
    pomniki = Attraction.objects.filter(category=3)
    parki = Attraction.objects.filter(category=4)
    zabytki = Attraction.objects.filter(category=5)
    rozrywkowe = Attraction.objects.filter(category=6)
    return render(request, 'attractions/atrakcje.html', {'lista_atrakcji': lista_atrakcji, 'muzea': muzea, 'sakralne': sakralne, 'pomniki': pomniki, 'parki': parki, 'zabytki': zabytki, 'rozrywkowe': rozrywkowe})


def wydarzenia(request,rok=datetime.now().year,miesiac=datetime.now().month,tmp=2):
    #lista_eventow = Event.objects.order_by('date')
    lista_eventow = Event.objects.filter(date__month=int(miesiac))

    if int(tmp)==0:
        if int(miesiac)==1:
            miesiac=12
            rok=int(rok)-1
        else:
            miesiac=int(miesiac)-1            
    elif int(tmp)==1:
        if int(miesiac)==12:
            miesiac=12
            rok=int(rok)+1
        else:
            miesiac=int(miesiac)+1
    
    htmlcode=MakeCalendar(rok,miesiac)
    return render(request, 'attractions/wydarzenia.html',{'lista_eventow': lista_eventow,'htmlcode':htmlcode, 'rok':rok, 'miesiac':miesiac})


def drogi(request):
    drogi = Route.objects.order_by('route_name')
    return render(request, 'attractions/drogi.html', {'drogi': drogi})


def droga(request, droga_id):
    url_moj = request.build_absolute_uri(reverse('drogi'))
    url_moj = url_moj+droga_id+'/'
    route = get_object_or_404(Route, pk=droga_id)
    return render(request, 'attractions/droga.html', {'droga': route, 'url_moj': url_moj})


def koszyk(request):
    atrakcje = Attraction.objects.all()
    return render(request, 'attractions/koszyk.html', {'atrakcje': atrakcje})



class UserFormView(View):
    form_class = UserForm
    template_name = 'attractions/rejestracja.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)

        username = form.data['login']
        email = form.data['mail']
        password = form.data['haslo']
        user = User.objects.create_user(username, email, password)
        user.save()
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return render(request, 'attractions/index.html', {})

        return render(request, self.template_name, {'form': form})


def MakeCalendar(year,month):
    tc= calendar.HTMLCalendar(firstweekday=0)

    htmlcode=tc.formatmonth(int(year),int(month))
    lista_eventow = Event.objects.filter(date__month=month)
    htmlEvents=""

    for x in range(1,31):
        mp=Event.objects.filter(date__month=month,date__day=x)
        htmlEvents=""
        if  len(mp)==0:
            continue
        for eventyWDzien in mp:
            htmlEvents=str(htmlEvents            
            +"<div><b>"
            +str(eventyWDzien.e_name)
            +"</b><br/><br/>"
            +str(eventyWDzien.description) 
            +"<br/><br/>"
            +"<b>Gdzie: </b>"
            + str(eventyWDzien.attraction.a_name)
            +"<br/><br/>"
            +"<b>Kiedy: </b>" 
            + str(eventyWDzien.date)          
            +" <br /><br />"		
            +"<b>O której: </b>"
            + str(eventyWDzien.open_time)
            +"<br /><br />"
            +"</div>")
        
        htmlcode=htmlcode.replace('>'
        +str(x)
        +'<', '>'
        +" <a href=\"javascript:void(0)\" onclick=\"document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'\">"
        #dzien miesiaca ponizej
        +str(x) 
        +"</a>"
        +'<div id=\"light\" class=\"white_content\">'
        +htmlEvents
        #powyzej content
        +"<a href=\"javascript:void(0)\" onclick=\"document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'\">Close</a>"
        +"</div>"
        +"<div id=\"fade\" class=\"black_overlay\"></div>"
        +'<')
    return htmlcode


def atrakcja_dodana(request, attr_id):
    attr = Attraction.objects.get(id=attr_id)
    bucket = request.session.get('bucket', {})
    bucket[attr_id] = attr.a_name
    request.session['bucket'] = bucket
    return render(request, 'attractions/atrakcja_dodana.html', {})

def atrakcja_usunieta(request, attr_id):
    attr = Attraction.objects.get(id=attr_id)
    bucket = request.session.get('bucket', {})
    try:
        del bucket[attr_id]
    except KeyError:
        pass
    request.session['bucket'] = bucket
    return render(request, 'attractions/atrakcja_usunieta.html', {})


def mapa(request):
    atrakcje = Attraction.objects.all()
    droga = Route()
    droga.save()
    droga.user = request.user
    droga.start = Attraction.objects.all()[:1].get()
    droga.end = Attraction.objects.latest('id')
    bucket = request.session.get('bucket', {})
    for attr in atrakcje:
        if attr.a_name in bucket.values():
            droga.attractions.add(attr)
    droga.start = droga.attractions.first()
    droga.end = droga.attractions.last()
    return render(request, 'attractions/mapa.html', {'atrakcje': atrakcje, 'droga': droga})
