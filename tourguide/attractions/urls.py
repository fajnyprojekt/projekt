from django.conf.urls import url
from django.contrib.auth import views as auth_view
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^attractions/$', views.atrakcje, name='atrakcje'),
    url(r'^attractions/add/(?P<attr_id>[0-9]+)/$', views.atrakcja_dodana, name='atrakcja_dodana'),
    url(r'^attractions/delete/(?P<attr_id>[0-9]+)/$', views.atrakcja_usunieta, name='atrakcja_usunieta'),
    url(r'^events/(?P<rok>[0-9]+)/(?P<miesiac>[0-9]+)/(?P<tmp>[0-9]+)/$', views.wydarzenia, name='wydarzenia'),
    url(r'^events/', views.wydarzenia, name='wydarzenia'),   
    url(r'^basket/$', views.koszyk, name='koszyk'),
    url(r'^basket/map/$', views.mapa, name='mapa'),
    url(r'^login/$', auth_view.login, name='login' ),
    url(r'^logout/$', auth_view.logout,{'next_page': '/'}, name='logout'),
    url(r'^registry/', views.UserFormView.as_view(), name='rejestracja'),
    url(r'^routes/$', views.drogi, name='drogi'),
    url(r'^routes/(?P<droga_id>[0-9]+)/', views.droga, name="droga"),
    url(r'^stworzpdf/(?P<droga_id>[0-9]+)$',views.stworzPDF, name="stworzpdf"),
]
