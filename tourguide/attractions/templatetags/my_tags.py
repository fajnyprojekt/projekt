from django import template

register = template.Library()


@register.filter(name='floatable')
def floatable(value):
    s = '{0:f}'.format(value)
    s = s.split(",")
    return ".".join(s)

@register.filter(name='get_item')
def get_item(dictionary, key):
    key = str(key)
    return dictionary.get(key)
