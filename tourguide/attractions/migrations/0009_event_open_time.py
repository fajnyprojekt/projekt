# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('attractions', '0008_auto_20170422_1410'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='open_time',
            field=models.TimeField(max_length=4, default=datetime.datetime.now),
        ),
    ]
