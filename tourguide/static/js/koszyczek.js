﻿
var atrakcje = new Array(6);
atrakcje[0]="";
atrakcje[1]="Pałac Branickich";
atrakcje[2]="Bazylika archikatedralna Wniebowzięcia Najświętszej Maryi Panny";
atrakcje[3]="Planty";
atrakcje[4]="";
atrakcje[5]="Ratusz";
atrakcje[6]="Teatr Dramatyczny im. Aleksandra Węgierki";
atrakcje[7]="Pomnik Bohaterów Ziemi Białostockiej";
atrakcje[8]="Murki";
atrakcje[9]="";
atrakcje[10]="Akcent ZOO";

var tablica = new Array()
tablica[0] = " ";
tablica[1] = " ";
tablica[2] = " ";
tablica[3] = " ";
tablica[4] = " ";
tablica[5] = " ";
tablica[6] = " ";
tablica[7] = " ";
tablica[8] = " ";
tablica[9] = " ";
tablica[10] = " ";

var napis = "";
var cala_tablica = "";
var pobrane_id = "";

function dodaj_do_koszyka(a)
{
	var element = atrakcje[a];
	tablica[a] = element + '<button class="cross_button" onclick="usun('+a+')"><img src="/static/images/cross.png" /></button>' + '<div style="height:10px;"></div>' ;
	wyswietl();
}

function usun(b)
{
	tablica[b]="";
	wyswietl();
}

function wyswietl()
{
	napis = "";
	for(i=0; i<tablica.length; i++)
	{
		napis = napis + tablica[i];
	}
	document.getElementById("tabela").innerHTML = napis;
	zapisz();
}

function zapisz()
{
	cala_tablica="";
	for(i=0; i<tablica.length; i++)
	{
		cala_tablica = cala_tablica + tablica[i] + "@";
	}
	var someVarName = cala_tablica;
	sessionStorage.setItem("someVarName", someVarName);
}

function start()
{
        var someVarName = sessionStorage.getItem("someVarName");
		document.getElementById("tabela").innerHTML = someVarName;
		if (someVarName != "")
        {
        	tablica = someVarName.split("@");
        }
        wyswietl();
}

/*Funkcja przechowuje id obiektów, które użytkownik włożył do koszyka - oddzielone są przecinkiem (",").
  Id jest zgodne z id obiektu w bazie.*/

function pobierz_id()
{
	pobrane_id="";
	for(i=0; i<tablica.length; i++)
	{
		if(tablica[i]!="" && tablica[i]!=" ")pobrane_id = pobrane_id + i + ",";
	}
}

window.onload = start