from django.test import TestCase
from .models import *
from django.test import Client
# Create your tests here.


class CategoryModelTests(TestCase):
    def setUp(self):
        Category.objects.create(c_name="c_test")

    def test_verbose_name_plural(self):
        self.assertEqual(str(Category._meta.verbose_name_plural), "Categories")

    def test_name(self):
        cat = Category.objects.get(c_name="c_test")
        self.assertEqual(cat.__str__(), cat.c_name)


class AttractionModelTests(TestCase):
    def setUp(self):
        Category.objects.create(c_name="c_test")
        Attraction.objects.create(a_name="a_test", price='-20')

    def test_name(self):
        att = Attraction.objects.get(a_name="a_test")
        self.assertEqual(att.__str__(), att.a_name)

    def test_not_negative_price(self):
        att = Attraction.objects.get(a_name="a_test")
        att.cannot_set_negative_price();
        self.assertEqual(att.price, '0')

class RouteModelTests(TestCase):
    def setUp(self):
        Route.objects.create(route_name="r_test")

    def test_name(self):
        rt = Route.objects.get(route_name="r_test")
        self.assertEqual(rt.__str__(), rt.route_name)


class ViewsTests(TestCase):
    def setUp(self):
        Route.objects.create(route_name="r_test")

    def test_attractions(self):
        response = self.client.get("/attractions/")
        self.assertEqual(response.status_code, 200)

    def test_events(self):
        response = self.client.get("/events/")
        self.assertEqual(response.status_code, 200)

    def test_routes(self):
        response = self.client.get("/routes/")
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = self.client.get("/login/")
        self.assertEqual(response.status_code, 200)

    def test_registration(self):
        response = self.client.get("/registry/")
        self.assertEqual(response.status_code, 200)

    def test_route_details(self):
        rt = Route.objects.get(route_name="r_text")
        response = self.client.get("/routes/"+rt.id+"/")
        self.assertEqual(response.status_code, 200)

